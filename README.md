My answers are rendered in **bold**.

1) The following code compiles on Linux. It has a number of problems, however.
Please locate as many of those problems as you are able and provide your
recommendations regarding how they can be resolved.

**My remarks are prefixed with "NOTE".**

``` c
// NOTE: we'd better include <stdlib.h>, to use malloc. Same for `printf`. Maybe, it is implied?

typedef struct list_s
{
	struct list_s *next; /* NULL for the last item in a list */
	int data;
}
list_t;

/* Counts the number of items in a list.
 */
int count_list_items(const list_t *head) {
	// NOTE: It does not handle `head == NULL` case
	if (head->next) {
		return count_list_items(head->next) + 1;  // NOTE: The recursive call will require more space in the stack, and with big enough lists, may even lead to stack overflow.
	} else {
		return 1;
	}
}

/* Inserts a new list item after the one specified as the argument.
 */
void insert_next_to_list(list_t *item, int data) {
	// NOTE: check `item != NULL`
	// NOTE: there is no check for `malloc` return value. What if it was unable to allocate memory?
	(item->next = malloc(sizeof(list_t)))->next = item->next;  // NOTE: the new element will point to itself, as a result.
	item->next->data = data;
}

/* Removes an item following the one specificed as the argument.
 */
void remove_next_from_list(list_t *item) {
	// NOTE: check `item != NULL`
	if (item->next) {
		free(item->next);
		item->next = item->next->next;  // NOTE: reference to a `free`d chunk of memory (`item->next->next`).
	}
}

/* Returns item data as text.
	*/
char *item_data(const list_t *list)
{
	// NOTE: check `list != NULL`
	char buf[12];

	sprintf(buf, "%d", list->data);
	return buf;  // NOTE: `buf` is allocated in the stack. It will have been deallocated by the time we return from the function.
}
```

2) By default, Linux builds user-space programs as dynamically linked
applications. Please describe scenarios where you would change the default
behavior to compile your application as a statically linked one.

**The answer**

- Global symbols. Linking against a static library results in that an
  executable, or a shared library being compiled, gets its own copy of the set
  of symbols defined in the static library;
	- In cases where we have, let's say, multiple shared libraries linked
	  against one commonly used library, we might want them to operate
	  independently from e/o, w/ their own chunks of memory;
- Dependency management. We might want to cut the number of dependencies, and
  make a portable executable, or a self-contained shared library;
- Speculation: cut off time expenses pertaining to library loading
	- Although UNIXes are supposed to only load a library once, at a program
	  startup, it still requires some time to invoke the library loader, to
	  lookup for library names, etc. Probably, there are some startup
	  time-critical applications where we might want to "milk" this iota of
	  performance.

3) Develop your version of the ls utility. Only the 'ls -l' functionality is
needed.

**The answer**

https://github.com/damurashov/AurLs
https://gitlab.com/damurashov/aurdmls

4) Please explain, at a very high-level, how a Unix OS, such as Linux,
implements the break-point feature in a debugger.

**The answer**

- *nix-es have a special system call named "ptrace", which enables a debugger
  process to attach to another process that is being debugged (debuggee);
- The debugger has r/w access to the debuggee's
	- Memory;
	- Registers;
- This enables the debugger to make code injection. Therefore, it is possible
  to inject an interrupt-generating instruction in an arbitrary position in the
  debuggee's memory space;
	- The debugger plants this instruction into a position that corresponds to
	  a certain line in the source file;
	- The reason the debuger "knows" exactly where to place a breakpoint is
	  that it has access to debug information, which makes it possible to
	  establish a relation b/w memory and source code instructions;
- When an interrupt is generated, the debugger gets notified by the OS, and the
  debuggee gets stopped;

5) Suppose you debug a Linux application and put a break-point into a function
defined by a dynamically-linked library used by the application. Will that
break-point stop any other application that makes use of the same library?

**The answer**

- No, because each process has its own memory space, independent from other
  processes;

6) Please translate in English. Don’t metaphrase, just put the idea as you see
it into English.

Если пользовательский процесс начал операцию ввода-вывода с устройством, и
остановился, ожидая окончания операции в устройстве, то операция в устройстве
закончится во время работы какого-то другого процесса в системе. Аппаратное
прерывание о завершении операции будет обработано операционной системой. Так
как на обработку прерывания требуется некоторое время, то текущий процесс в
однопроцессорной системе будет приостановлен. Таким образом, любой процесс в
системе непредсказуемо влияет на производительность других процессов независимо
от их приоритетов.

**The answer**

In the case when a process has started an I/O procedure, and waits for it to
finish, there usually are other processes running in the system. So at the
moment the IO operation is finished, a hardware interrupt notifying I/O
completion is generated, and an ISR is called. Neither the ISR duration, nor
the time when the interrupt is generated are predictable. As a result, each
process running in the system introduces unpredictability, and affects other
processes' timing in an unforeseeable way regardless of priorities
